package com.javatechie.crud.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;


@SpringBootTest
class SpringBootCrudExample2ApplicationTests {
	
	@Test
	void a_books() {

    	RestTemplate restTemplate = new RestTemplate();
        String uri_rcd = "http://localhost:9191/books";

	    String result = restTemplate.getForObject(uri_rcd, String.class);	
	    
	    Assertions.assertTrue(result.indexOf("200 OK") == 0);
	    
	    System.out.println("books result : " + result);
	}
	
	@Test
	void b_createUser() {
    	RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:9191/users";
        		
		String requestJson = "{" + 
				"    \"userName\": \"name14.surname14\"," + 
				"    \"password\": \"pass\"," + 
				"    \"date_of_birth\":\"14/10/2020\"" + 
				"}";
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(requestJson,headers);
		
		String result = restTemplate.postForObject(url, entity, String.class);
	    
	    Assertions.assertTrue(result.indexOf("200 OK") == 0);
	    
	    System.out.println("createUser result : " + result);
	}
	
	@Test
	void c_login() {
    	RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:9191/login";
        		
		String requestJson = "{ "
				+ "\"userName\": \"user2.surman2\", " 
				+ " \"password\": \"pass\" "
				+ "}";
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(requestJson,headers);
		
		String result = restTemplate.postForObject(url, entity, String.class);
	    
	    Assertions.assertTrue(result.indexOf("200 OK") == 0);
	    
	    System.out.println("login result : " + result);
	}
	
//	@Test
//	void getUser() {
//    	RestTemplate restTemplate = new RestTemplate();
//        String url = "http://localhost:9191/login";
//        		
//		String requestJson = "{ "
//				+ "\"userName\": \"user2.surman2\", " 
//				+ " \"password\": \"pass\" "
//				+ "}";
//		
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_JSON);
//		HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);
//		
//		String result = restTemplate.postForObject(url, entity, String.class);
//	    
//	    Assertions.assertTrue(result.indexOf("200 OK") == 0);
//	    
//        String uri = "http://localhost:9191/users";
//
//	    result = restTemplate.getForObject(uri, String.class);	
//	    
//	    Assertions.assertTrue(result.indexOf("200 OK") == 0);
//	    
//	    System.out.println("getUser result : " + result);
//	}
}
