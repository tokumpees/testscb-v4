package com.javatechie.crud.example.service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.javatechie.crud.example.entity.Book;
import com.javatechie.crud.example.entity.Info;
import com.javatechie.crud.example.entity.Login;
import com.javatechie.crud.example.entity.Order;
import com.javatechie.crud.example.entity.User;
import com.javatechie.crud.example.repository.OrderRepository;
import com.javatechie.crud.example.repository.UserRepository;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

@Service
public class UserService {
    @Autowired
    private UserRepository repository;
    @Autowired
    private OrderRepository orderRepository;

    public User saveUser(User user) {
        return repository.save(user);
    }

    public List<User> saveUsers(List<User> users) {
        return repository.saveAll(users);
    }

    public List<User> getUsers() {
        return repository.findAll();
    }

    public User getUserById(int id) {
        return repository.findById(id).orElse(null);
    }

    public User getLogin(Login login) throws Exception {
    	if(null == login.getUserName()) {
    		throw new Exception("Invalid user or password");
    	}
    	
    	if(null == login.getPassword()) {
    		throw new Exception("Invalid user or password");
    	}    	
    	
    	User user = repository.findByUserName(login.getUserName());

    	if(null == user) {
    		throw new Exception("Invalid user or password");
    	}
    	
        byte[] encryptArray = Base64.encodeBase64(login.getPassword().getBytes());        
        String encPassword = new String(encryptArray,"UTF-8");   
            	
    	if(!user.getPassword().equals(encPassword)) {
    		throw new Exception("Invalid user or password");
    	}
    	
        return user;
    }

    public User create(Login login) throws Exception {

    	if(null == login) {
    		throw new Exception("Invalid user data");
    	}

    	if(StringUtils.isEmpty(login.getPassword())) {
    		throw new Exception("Invalid password");
    	}
    	

        byte[] encryptArray = Base64.encodeBase64(login.getPassword().getBytes());        
        String encPassword = new String(encryptArray,"UTF-8"); 
            	
    	if(StringUtils.isEmpty(login.getDate_of_birth())) {
    		throw new Exception("Invalid Date_of_birth");
    	}
    	    	
    	User user = repository.findByUserName(login.getUserName());
    	
    	if(null != user) {
    		throw new Exception("Duplicate user name");
    	}
    	
    	String userName = login.getUserName();

    	String[] name = userName.split("\\.");
    	    	
    	if(null == name || name.length < 2) {
    		throw new Exception("Invalid username format(name.surname)");
    	}
    	    	
    	user = new User();
    	user.setName(name[0]);
    	user.setSurname(name[1]);
    	user.setUserName(userName);
    	user.setPassword(encPassword);
    	user.setDateOfBirth(login.getDate_of_birth());
    	
        return repository.save(user);
    	
    }

    public void delete(User user) throws Exception {
        repository.delete(user);
    }

    public User getUserCurrent(int id) {
        return repository.findById(id).orElse(null);
    }

    public User getUserByName(String name) {
        return repository.findByName(name);
    }

    public User getUserByUserName(String userName) {
        return repository.findByUserName(userName);
    }

    public String deleteUser(int id) {
        repository.deleteById(id);
        return "user removed !! " + id;
    }

    public User updateNameSurName(User user) {
        User existingUser = repository.findById(user.getId()).orElse(null);
        existingUser.setName(user.getName());
        existingUser.setSurname(user.getSurname());
        return repository.save(existingUser);
    }

    public User updateStoreInfo(User user) {
        User existingUser = repository.findById(user.getId()).orElse(null);
        existingUser.setStoreInfo(user.getStoreInfo());
        return repository.save(existingUser);
    }
    
    public Book bookList() throws Exception {

    	RestTemplate restTemplate = new RestTemplate();
        String uri_rcd = "https://scb-test-book-publisher.herokuapp.com/books/recommendation";

	    String result = restTemplate.getForObject(uri_rcd, String.class);	    
	    JsonArray jsonArray_rcd = new Gson().fromJson(result, JsonArray.class);
	    
        String uri = "https://scb-test-book-publisher.herokuapp.com/books";
        
	    result = restTemplate.getForObject(uri, String.class);	    
	    JsonArray jsonArray = new Gson().fromJson(result, JsonArray.class);
	    
	    List sortBooks = sortJson(jsonArray_rcd, jsonArray);
	    
	    Book books= new Book();
	    books.setBooks(sortBooks);
	    
	    return books;
	    
    }
    
    public Map<String, JsonObject> getMapBook() throws Exception {
    	RestTemplate restTemplate = new RestTemplate();
        String uri = "https://scb-test-book-publisher.herokuapp.com/books";
        
        String result = restTemplate.getForObject(uri, String.class);	    
	    JsonArray jsonArray = new Gson().fromJson(result, JsonArray.class);
	    
		Map<String, JsonObject> mapJson = new TreeMap();
		
		for (int i = 0; i < jsonArray.size(); i++) {
			String id = jsonArray.get(i).getAsJsonObject().get("id").toString();

			mapJson.put(id, jsonArray.get(i).getAsJsonObject());
		}
	    
	    return mapJson;	    
    }
    
	public List sortJson(JsonArray jsonArray_rcd, JsonArray jsonArray) throws Exception {

		List<JsonObject> sortList_rcd = new ArrayList();
		Map<String, JsonObject> mapJson_rcd = new TreeMap();
		

		for (int i = 0; i < jsonArray_rcd.size(); i++) {
			String id = jsonArray_rcd.get(i).getAsJsonObject().get("id").toString();
			String book_name = jsonArray_rcd.get(i).getAsJsonObject().get("book_name").toString();

			mapJson_rcd.put(book_name + id, jsonArray_rcd.get(i).getAsJsonObject());
		}

		SortedSet<String> keys = new TreeSet<>(mapJson_rcd.keySet());
		for (String key : keys) {
			JsonObject jsonObject = (JsonObject) mapJson_rcd.get(key);			
			sortList_rcd.add(jsonObject);
			
//			System.out.println("1 Sorted jsonObject: " + jsonObject);
		}

		List<JsonObject> sortList = new ArrayList();
		Map<String, JsonObject> mapJson = new TreeMap();
		

		for (int i = 0; i < jsonArray.size(); i++) {
			String id = jsonArray.get(i).getAsJsonObject().get("id").toString();
			String book_name = jsonArray.get(i).getAsJsonObject().get("book_name").toString();

//			System.out.println(id);
//			System.out.println(book_name);
			
			if(!mapJson_rcd.containsKey(book_name + id)) {
				mapJson.put(book_name + id, jsonArray.get(i).getAsJsonObject());
			}
		}

		keys = new TreeSet<>(mapJson.keySet());
		for (String key : keys) {
			JsonObject jsonObject = (JsonObject) mapJson.get(key);			
			sortList.add(jsonObject);
			
//			System.out.println("2 Sorted jsonObject: " + jsonObject);
		}
		
		sortList_rcd.addAll(sortList);
		
		return sortList_rcd;
	}
	
    public String order(String userId, Map<String, JsonObject> mapBook, Book orderBook) throws Exception {
		User user = getUserById(Integer.parseInt(userId));

    	if(null == user) {
    		throw new Exception("Invalid user data");
    	}

    	if(null == mapBook) {
    		throw new Exception("Invalid book data");
    	}

    	if(null == orderBook) {
    		throw new Exception("Invalid book order");
    	}
    	
    	List<String> bookOrder = orderBook.getOrders();
    	
    	if(bookOrder.size() == 0) {
    		throw new Exception("Invalid book order");
    	}
    	
    	for(String bookId : bookOrder) {        	
        	if(mapBook.containsKey(bookId)) {
        		//
        	} else {
        		throw new Exception("Invalid book Id = " + bookId);
        	}
    	}
    	
    	BigDecimal totalPrice = new BigDecimal(0);
    	
    	for(String bookId : bookOrder) {        	
        	if(mapBook.containsKey(bookId)) {
    			JsonObject jsonObject = (JsonObject) mapBook.get(bookId);			
    			String price = jsonObject.get("price").toString();
    			
    			BigDecimal bdPrice = new BigDecimal(price);    			
    			totalPrice = totalPrice.add(bdPrice);
        		
        		Order order = new Order();
        		order.setBookId(bookId);
        		order.setStoreInfo(user.getStoreInfo());
        		order.setPrice(price);
        		
        		orderRepository.save(order);        		
        		
        	}
    	}
    	
        return totalPrice.toString();    	
    }
}
