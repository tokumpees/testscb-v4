package com.javatechie.crud.example.service;

import com.javatechie.crud.example.entity.Order;
import com.javatechie.crud.example.repository.OrderRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepository repository;

    public Order saveOrder(Order product) {
        return repository.save(product);
    }

    public List<Order> saveOrders(List<Order> order) {
        return repository.saveAll(order);
    }

    public List<Order> getOrders() {
        return repository.findAll();
    }

    public Order getOrderById(int id) {
        return repository.findById(id).orElse(null);
    }
    
    public Order getOrderByStoreInfo(String storeInfo) {
        return repository.findByStoreInfo(storeInfo);
    }
    
    public List<Order> getAllOrderByStoreInfo(String storeInfo) {
        return repository.findAllByStoreInfo(storeInfo);
    }
    
    public List<Integer> getAllBookId(String storeInfo) {
    	List<Order> orders = repository.findAllByStoreInfo(storeInfo);
    	
    	List<Integer> books = new ArrayList<Integer>();
    	
    	for(Order order : orders) {
    		books.add(Integer.valueOf(order.getBookId()));
    	}
    	
        return books;
    }

    public String deleteByStoreInfo(String storeInfo) {
    	List<Order> orders = repository.findAllByStoreInfo(storeInfo);

    	for(Order order : orders) {
    		deleteOrder(order.getId());
    	}
    	
        return "Order removed by StoreInfo !! " + storeInfo;
    }

    public String deleteOrder(int id) {
        repository.deleteById(id);
        return "Order removed !! " + id;
    }

    public Order updateOrder(Order order) {
        Order existingOrder = repository.findById(order.getId()).orElse(null);
        existingOrder.setBookId(order.getBookId());
        existingOrder.setStoreInfo(order.getStoreInfo());
        existingOrder.setPrice(order.getPrice());
        return repository.save(existingOrder);
    }


}
