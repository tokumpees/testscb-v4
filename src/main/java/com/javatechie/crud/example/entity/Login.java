package com.javatechie.crud.example.entity;

public class Login {
    private String userName;
    private String password;
    private String date_of_birth;

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDate_of_birth() {
		return date_of_birth;
	}
	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}
	@Override
	public String toString() {
		return "Login [userName=" + userName + ", password=" + password + ", date_of_birth=" + date_of_birth + "]";
	}
	
    
}
