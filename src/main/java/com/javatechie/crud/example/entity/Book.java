package com.javatechie.crud.example.entity;

import java.util.List;

public class Book {
    private List<String> orders;
    private List<String> books;
    
	public List<String> getOrders() {
		return orders;
	}

	public void setOrders(List<String> orders) {
		this.orders = orders;
	}

	public List<String> getBooks() {
		return books;
	}

	public void setBooks(List<String> books) {
		this.books = books;
	}

	@Override
	public String toString() {
		return "Book [books=" + books + "]";
	}
    
}
