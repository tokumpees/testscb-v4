package com.javatechie.crud.example.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.javatechie.crud.example.entity.Book;
import com.javatechie.crud.example.entity.Info;
import com.javatechie.crud.example.entity.Login;
import com.javatechie.crud.example.entity.User;
import com.javatechie.crud.example.service.OrderService;
import com.javatechie.crud.example.service.UserService;

@RestController
public class UserController {

    @Autowired
    private UserService service;
    @Autowired
    private OrderService ordservice;

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody Login login, HttpServletRequest request)  throws Exception {
    	
    	try {
    		User user = service.getLogin(login);
    		
    		request.getSession().setAttribute("MY_SESSION_MESSAGES", String.valueOf(user.getId()));
    		
    		String messages = (String) request.getSession().getAttribute("MY_SESSION_MESSAGES");
    		
    		System.out.println("userid = " + messages);
    		
    		Map<String, JsonObject> mapBook = service.getMapBook();
    		
    		request.getSession().setAttribute("MY_SESSION_BOOK", mapBook);
    		
    		Map<String, JsonObject> sessMapBook = (Map<String, JsonObject>) request.getSession().getAttribute("MY_SESSION_BOOK");
    		
    		System.out.println("mapBook = " + sessMapBook);

    		
    	} catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage() + "\n");
		}
    	
        return ResponseEntity.status(HttpStatus.OK).body(HttpStatus.OK + "\n");
    }

    @GetMapping("/users")
    public ResponseEntity<String> user(HttpServletRequest request)  throws Exception {
    	String userId = null;
    	
    	try {
    		userId = (String) request.getSession().getAttribute("MY_SESSION_MESSAGES");
    		
    		if(null == userId) {
    			throw new Exception();
    		}
    	
    	} catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Please login." + "\n");
		}
    		
    	try {    		
    		User user = service.getUserById(Integer.parseInt(userId));
    		List<Integer> books = ordservice.getAllBookId(user.getStoreInfo());
    		
    		System.out.println("user = " + user.toString());
    		    		
    		Info info = new Info();
    		info.setName(user.getName());
    		info.setSurname(user.getSurname());
    		info.setDate_of_birth(user.getDateOfBirth());
    		info.setBooks(books);
			String jsonInString = new Gson().toJson(info);

            return ResponseEntity.status(HttpStatus.OK).body(HttpStatus.OK + "\n" + jsonInString);
    		
    	} catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage() + "\n");
		}
    }

    @DeleteMapping("/users")
    public ResponseEntity<String> delete(HttpServletRequest request)  throws Exception {
    	String userId = "";
    	
    	try {
    		userId = (String) request.getSession().getAttribute("MY_SESSION_MESSAGES");
    		
    		if(null == userId) {
    			throw new Exception();
    		}
    	
    	} catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Please login." + "\n");
		}
    		
    	try {    		
    		User user = service.getUserById(Integer.parseInt(userId));
    		
    		ordservice.deleteByStoreInfo(user.getStoreInfo());
    		service.delete(user);

            return ResponseEntity.status(HttpStatus.OK).body(HttpStatus.OK + "\n");
    		
    	} catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage() + "\n");
		}
    }
    
    @PostMapping("/users")
    public ResponseEntity<String> add(@RequestBody Login login)  throws Exception {   	
    	try {
    		User user = service.create(login);
    		user.setStoreInfo(user.getId()+"");
    		service.updateStoreInfo(user);

            return ResponseEntity.status(HttpStatus.OK).body(HttpStatus.OK + "\n");

    	} catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage() + "\n");
		}    		
    }

	@PostMapping("/users/orders")
	public ResponseEntity<String> order(@RequestBody Book book, HttpServletRequest request) throws Exception {
		String userId = "";
		Map<String, JsonObject> sessMapBook = null;

		try {
			userId = (String) request.getSession().getAttribute("MY_SESSION_MESSAGES");
    		
    		if(null == userId) {
    			throw new Exception();
    		}

		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Please login." + "\n");
		}

		try {
			sessMapBook = (Map<String, JsonObject>) request.getSession().getAttribute("MY_SESSION_BOOK");

		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid book data." + "\n");
		}

		try {
			String totalPrice = service.order(userId, sessMapBook, book);

			return ResponseEntity.status(HttpStatus.OK).body(HttpStatus.OK + "\n" + totalPrice);

		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage() + "\n");
		}
	}
 
	@GetMapping("/books")
    public ResponseEntity<String> books(HttpServletRequest request)  throws Exception { 
	    try {
	    	Book books = service.bookList();
	    	
			String jsonInString = new Gson().toJson(books);

            return ResponseEntity.status(HttpStatus.OK).body(HttpStatus.OK + "\n" + jsonInString);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage() + "\n");
       }    	    	
    }
	

	@GetMapping("/")
	public String process(Model model, HttpSession session) {
		@SuppressWarnings("unchecked")
		List<String> messages = (List<String>) session.getAttribute("MY_SESSION_MESSAGES");

		if (messages == null) {
			messages = new ArrayList<>();
		}
		model.addAttribute("sessionMessages", messages);

		return "index";
	}

	@PostMapping("/persistMessage")
	public String persistMessage(@RequestParam("msg") String msg, HttpServletRequest request) {
		@SuppressWarnings("unchecked")
		List<String> messages = (List<String>) request.getSession().getAttribute("MY_SESSION_MESSAGES");
		if (messages == null) {
			messages = new ArrayList<>();
			request.getSession().setAttribute("MY_SESSION_MESSAGES", messages);
		}
		messages.add(msg);
		request.getSession().setAttribute("MY_SESSION_MESSAGES", messages);
		return "redirect:/";
	}

	@PostMapping("/destroy")
	public String destroySession(HttpServletRequest request) {
		request.getSession().invalidate();
		return "redirect:/";
	}

}
