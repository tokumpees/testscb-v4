package com.javatechie.crud.example.repository;

import com.javatechie.crud.example.entity.Order;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order,Integer> {
    Order findById(String id);
    Order findByStoreInfo(String storeInfo);
    List<Order> findAllByStoreInfo(String storeInfo);
}

